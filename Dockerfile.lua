FROM debian:9.13 as build

RUN apt update && \
apt upgrade -y && \
apt install -y wget libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev checkinstall build-essential autoconf automake

RUN wget -P /tmp https://openresty.org/download/openresty-1.19.9.1.tar.gz && \
cd /tmp && tar xfv openresty-1.19.9.1.tar.gz

RUN cd /tmp/openresty-1.19.9.1&& ./configure && \
make && make install

FROM debian:9.13-slim as run

RUN apt update && \
apt install -y net-tools procps git vim && \
apt-get clean -y && \
apt-get autoclean -y && \
rm -rf var/cache/*

COPY --from=build /usr/local/openresty /usr/local/openresty
COPY --from=build /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1 /lib
COPY --from=build /usr/local/openresty/luajit/lib/libluajit-5.1.so.2 /lib
COPY --from=build /usr/lib/x86_64-linux-gnu/libssl.so.1.1 /lib
COPY --from=build /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1 /usr/local/lib

WORKDIR /usr/local/openresty/nginx/sbin/

CMD ["./nginx","-g","daemon off;"]