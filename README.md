# less1.4

## Задание

```
Необходимо собрать nginx из исходников со стандартными модулями и запустить бинарник во втором образе.
Образы Debian9
При запуске примаунтить конфиг в контейнер.
```

## Решение

1. делаю multistage сборку образа. Будет две стадии: 1 - где будет находится компилятор и происходить сборка, 2 - легкий образ, куда скопируется бинарник с nginx и небходимые зависимости
2. в репе будет находиться конифг nginx, который будет примаунтиваться внутрь контейнера
3. команды для запуска сборки и запуска контейнера с nginx
```
# сборка
docker build -t nginx:latest -f Dockerfile .

# запуск
docker run -v "$(pwd)"/nginx.conf:/etc/nginx/nginx.conf \
  -p 80:8080 -d --name nginx nginx
```

```
docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS         PORTS                                   NAMES
dd28aeaf0b80   nginx:latest   "./nginx -g 'daemon …"   8 seconds ago   Up 7 seconds   0.0.0.0:80->8080/tcp, :::80->8080/tcp   nginx
```

```
curl 127.0.0.1
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
...
```
подключенные модули
```
nginx -V
nginx version: nginx/1.19.9
built by gcc 6.3.0 20170516 (Debian 6.3.0-18+deb9u1)
built with OpenSSL 1.1.0l  10 Sep 2019
TLS SNI support enabled
configure arguments: --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-cc-opt='-g -O2 -fdebug-prefix-map=/data/builder/debuild/nginx-1.19.10/debian/debuild-base/nginx-1.19.10=. -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' --with-ld-opt='-Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie'
```

### добавление модуля для lua

У меня не получилось добавить модули для lua к сборки из исходников nginx, поэтому я использовал исходный код от openresty

```
# сборка
docker build -t lua -f Dockerfile.lua .

# запуск
docker run -v "$(pwd)"/index.lua:/usr/local/openresty/nginx/html/index.lua \
  -v "$(pwd)"/nginx_lua.conf:/usr/local/openresty/nginx/conf/nginx.conf \
  -p 81:8080 -d --name nginx_lua lua
```

```
docker ps
CONTAINER ID   IMAGE        COMMAND                  CREATED         STATUS         PORTS                                   NAMES
8908e66cb855   lua:latest   "./nginx -g 'daemon …"   8 minutes ago   Up 8 minutes   0.0.0.0:81->8080/tcp, :::81->8080/tcp   nginxlua
```

```
root@novoleg-vm-00:~/nginx# curl 127.0.0.1:81
Hello, Anonymous!
```

### Сборка lua с обычным nginx

```
# сборка
docker build -t bad -f Dockerfile.lua.bad .

# запуск

docker run -v "$(pwd)"/index.lua:/usr/local/openresty/nginx/html/index.lua \
  -v "$(pwd)"/nginx_lua.conf:/usr/local/openresty/nginx/conf/nginx_lua_bad.conf \
  -p 82:8080 -d --name nginxlua bad
```

Вот такая ошибка появляется
```
nginx: [alert] failed to load the 'resty.core' module (https://github.com/openresty/lua-resty-core); ensure you are using an OpenResty release from https://openresty.org/en/download.html (reason: ...nginx/sbin/lua-resty-core-0.1.20/lib/resty/core/base.lua:24: ngx_http_lua_module 0.10.18 required) in /usr/local/nginx/conf/nginx.conf:35
```

Пытался различные версии качать lua-resty-core, то версия не та, то найти файлы не может...

### Мне показали рабочий вариант, когда nginx собирается из исходников в lua модулями

```
# сборка
docker build -t good -f Dockerfile.lua.good .

# запуск

docker run -v -v "$(pwd)"/nginx_lua_bad.conf:/usr/local/nginx/conf/nginx.conf \
  -v "$(pwd)"/index.lua:/usr/local/nginx/html/index.lua good \
  -p 83:8080 -d --name nginxluagood good
```
