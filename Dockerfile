FROM debian:9.13 as build

RUN apt update && \
apt upgrade -y && \
apt install -y wget libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev checkinstall build-essential autoconf automake

RUN wget -P /tmp http://nginx.org/download/nginx-1.19.9.tar.gz && \
cd /tmp && tar xfv nginx-1.19.9.tar.gz

###
# build nginx
###
RUN cd /tmp/nginx-1.19.9 && \ 
./configure --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx \
--modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf \
--user=nginx --group=nginx --with-compat --with-file-aio --with-threads \
--with-http_addition_module --with-http_auth_request_module \
--with-http_dav_module --with-http_flv_module --with-http_gunzip_module \
--with-http_gzip_static_module --with-http_mp4_module \
--with-http_random_index_module --with-http_realip_module \
--with-http_secure_link_module --with-http_slice_module \
--with-http_ssl_module --with-http_stub_status_module \
--with-http_sub_module --with-http_v2_module \
--with-mail --with-mail_ssl_module --with-stream \
--with-stream_realip_module --with-stream_ssl_module \
--with-stream_ssl_preread_module \
--with-cc-opt='-g -O2 -fdebug-prefix-map=/data/builder/debuild/nginx-1.19.10/debian/debuild-base/nginx-1.19.10=. -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' \
--with-ld-opt='-Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie' && \
make && make install

FROM debian:9.13-slim as run

RUN apt update && \
apt install -y net-tools procps && \
apt-get clean -y && \
apt-get autoclean -y && \
useradd --no-create-home nginx && \
rm -rf var/cache/*

COPY --from=build /usr/sbin/nginx /usr/sbin/nginx
COPY --from=build /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1 /lib
COPY --from=build /usr/lib/x86_64-linux-gnu/libssl.so.1.1 /lib
COPY --from=build /etc/nginx /etc/nginx

CMD ["nginx","-g","daemon off;"]